//
//  ViewController.swift
//  uBeer
//
//  Created by Tareq Saifullah on 19/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, FirstViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tempPop = [Project]()
    let projectCellId = "projectCell"
    let newProjectCellId = "projectCreationCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.7961, green: 0.6941, blue: 0.2235, alpha: 1.0) /* #cbb139 */
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "birraSfondo")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        view.insertSubview(backgroundImage, at: 0)
        
        collectionView.backgroundColor = .clear
        
        tempPop = CoreDataQueries.shareInstance.allProjects()
        
        //let products = CoreDataQueries.shareInstance.allProducts()
        
        collectionView.register(newProjectCell.self, forCellWithReuseIdentifier: newProjectCellId)
        collectionView.register(projectCell.self, forCellWithReuseIdentifier: projectCellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tempPop.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: newProjectCellId, for: indexPath) as! newProjectCell
            cell.layer.cornerRadius = 15
            cell.mask?.clipsToBounds = true
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: projectCellId, for: indexPath) as! projectCell
            cell.layer.cornerRadius = 15
            cell.mask?.clipsToBounds = true
            cell.projectImageView.image = UIImage(named: tempPop[indexPath.item - 1].beer!.image!)
            cell.projectTitleLabel.text = tempPop[indexPath.item - 1].name
            return cell
        }
    }
    
    
    let topBottomPadding: CGFloat = 20
    let LeadingTrailingPadding: CGFloat = 40
    let lineSpacing: CGFloat = 20
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (view.frame.width / 2.7), height: (view.frame.width / 2.7))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 85, left: LeadingTrailingPadding, bottom: topBottomPadding, right: LeadingTrailingPadding)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            let newbeerVC = self.storyboard?.instantiateViewController(identifier: "newbeervc") as! NewBeerViewController
            newbeerVC.firstView = self
            self.present(newbeerVC, animated: true) {
//                This completion handler disables the gesture recognizer to not swipe the modal view down
                newbeerVC.presentationController?.presentedView?.gestureRecognizers?.first?.isEnabled = false
            }
        default:
            let tasksVC = self.storyboard?.instantiateViewController(identifier: "TasksViewController") as! TasksViewController
            tasksVC.project = tempPop[indexPath.item - 1]
            self.navigationController?.pushViewController(tasksVC, animated: true)
        }
    }
    func updateArray (projects: [Project]) {
        tempPop = projects
        collectionView.reloadData()
    }
    
}

