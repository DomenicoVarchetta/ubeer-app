//
//  Model.swift
//  uBeer
//
//  Created by Tareq Saifullah on 04/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation

struct IngredientType {
    let name:String
    var isChecked:Bool
}
struct TaskType {
    let name:String
    let info:String
    var isDone:Bool
}
struct BeerType{
    let name:String
    var ingredients:[IngredientType]
    var tasks:[TaskType]
    let image:String
}
