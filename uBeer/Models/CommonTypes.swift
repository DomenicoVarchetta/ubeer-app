//
//  CommonTypes.swift
//  uBeer
//
//  Created by Domenico Varchetta on 07/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation

//Stout
let materialsStout = [
    IngredientType(name: "Water (15 lt)", isChecked: false) ,
    IngredientType(name: "Pilsener Malt (5 Kg)", isChecked: false),
    IngredientType(name: "Pauls Maris Otter Malt (4 kg)", isChecked: false) ,
    IngredientType(name: "Roasted Barley Malt (350 g)", isChecked: false),
    IngredientType(name: "Pauls Chocolate Malt (120 g)", isChecked: false) ,
    IngredientType(name: "Pauls Amber Malt (120 g)", isChecked: false),
    IngredientType(name: "Sparge (12 liters)", isChecked: false) ,
       IngredientType(name: "Challenger Hop (28g + 7g)", isChecked: false),
       IngredientType(name: "Brown Sugar (95g)", isChecked: false) ,
       IngredientType(name: "Yeast M15 Empire Ale 2 Pack", isChecked: false)
]

var worksStout = [
    TaskType(name: "Mash Phase",info:"Mashing time: 60 min + 15 min", isDone: false ),
    TaskType(name: "Mash Phase 1",info: "Temperature: 60 min @68°C + 15 min @78°C", isDone: false),
    TaskType(name: "Mash Phase 2",info: "Add Pilsener Malt", isDone: false),
    TaskType(name: "Mash Phase 3",info: "Add Pauls Maris Otter Malt", isDone: false),
    TaskType(name: "Mash Phase 4",info: "Add Roasted Barley Malt", isDone: false),
    TaskType(name: "Mash Phase 5",info: "Add Pauls Chocolate Malt", isDone: false),
    TaskType(name: "Mash Phase 6",info: "Add Pauls Amber Malt", isDone: false),
    TaskType(name: "Boiling Phase",info: "Boiling Time: 70 min", isDone: false),
    TaskType(name: "Boiling Phase 1",info: "Sparge: 12 lt", isDone: false),
       TaskType(name: "Boiling Phase 2",info: "Add Challenger Hop: 28g + 7g", isDone: false),
       TaskType(name: "Fermentation Phase",info: "Add Brown Sugar", isDone: false),
       TaskType(name: "Fermentation Phase 1",info: "24 °C 2 Days & +28 °C till the end of fermentation", isDone: false),
       TaskType(name: "Fermentation Phase 2",info: "Maturation: 1 week at 2 °C", isDone: false)
]

//Weisse
let materialsWeisse = [
    IngredientType(name: "Water (16 liters)", isChecked: false) ,
    IngredientType(name: "Protein Rest", isChecked: false),
    IngredientType(name: "Ireks Weizen Hell (3 Malt: 2,5 gg)", isChecked: false),
    IngredientType(name: "Ireks Pilsner Malt (2,4 kg)", isChecked: false),
    IngredientType(name: "Ireks Karamell WasserHell Malt (200 g)", isChecked: false),
    IngredientType(name: "Sparge (14 liters)", isChecked: false),
    IngredientType(name: "Hersbrucker 3.2% Hop (25g)", isChecked: false),
    IngredientType(name: "Tettnang 2,2% Hop (15g)", isChecked: false),
    IngredientType(name: "Brown Sugar (138g)", isChecked: false),
    IngredientType(name: "Yeast M20 2 Pack", isChecked: false),
]

var worksWeisse = [
    TaskType(name: "Mash Phase",info:"Water: 16 lt", isDone: false ),
    TaskType(name: "Mash Phase 1",info: "Protein Rest: 15 min @ 52 °C", isDone: false),
    TaskType(name: "Mash Phase 2",info: "Mashing time: 50 min + 15 min", isDone: false),
    TaskType(name: "Mash Phase 3",info: "Temperature: 50 min @66°C + 15 min @78°C", isDone: false),
    TaskType(name: "Mash Phase 4",info: "Add Ireks Weizen Hell 3 Malt", isDone: false),
    TaskType(name: "Mash Phase 5",info: "Add Ireks Pilsner Malt", isDone: false),
    TaskType(name: "Mash Phase 5",info: "Add Ireks Karamell WasserHell Malt", isDone: false),
    TaskType(name: "Boiling Phase",info: "Sparge: 14 liters. Boiling Time: 60 minutes", isDone: false),
    TaskType(name: "Boiling Phase 1",info: "Add Hersbrucker 3.2% Hop", isDone: false),
    TaskType(name: "Boiling Phase 2",info: "Add Tettnang 2,2% Hop", isDone: false),
    TaskType(name: "Fermentation Phase",info: "Add Brown Sugar", isDone: false),
    TaskType(name: "Fermentation Phase 1",info: "17°C till the end of fermentation", isDone: false),
    TaskType(name: "Fermentation Phase 2",info: "Maturation: 1 week at 2 °C", isDone: false)
]

//IPA
let materialsIpa = [
    IngredientType(name: "Water (15 liters)", isChecked: false) ,
    IngredientType(name: "Ireks Pale Ale Malt (4 kg)", isChecked: false),
    IngredientType(name: "Ireks Monaco Malt (500 g)", isChecked: false),
    IngredientType(name: "Pauls Crystal Medium Malt (300 g)", isChecked: false),
    IngredientType(name: "Chateau Melano Malt (200 g)", isChecked: false),
    IngredientType(name: "Sparge (10 liters)", isChecked: false),
    IngredientType(name: "Northern Brewer Hop (20g + 20g + 20g)", isChecked: false),
    IngredientType(name: "Yeast M84 2 Pack", isChecked: false)
]

var worksIpa = [
    TaskType(name: "Mash Phase",info:"Water: 16 lt", isDone: false ),
    TaskType(name: "Mash Phase 1",info: "Mashing time: 60 min + 15 min", isDone: false),
    TaskType(name: "Mash Phase 2",info: "Temperature: 60 min @66°C + 15 min @78°C", isDone: false),
    TaskType(name: "Mash Phase 3",info: "Add Ireks Pale Ale Malt", isDone: false),
    TaskType(name: "Mash Phase 4",info: "Add Ireks Monaco Malt", isDone: false),
    TaskType(name: "Mash Phase 5",info: "Add Pauls Crystal Medium Malt:", isDone: false),
    TaskType(name: "Mash Phase 6",info: "Chateau Melano Malt", isDone: false),
    TaskType(name: "Boiling Phase",info: "Boiling Time: 70 min", isDone: false),
    TaskType(name: "Boiling Phase 1",info: "Add Northern Brewer Hop: 20g + 20g + 20g", isDone: false),
    TaskType(name: "Fermentation Phase",info: "17 °C till the end of fermentation", isDone: false)
]

//belgian-Saison
let materialsBelgian = [
    IngredientType(name: "Water (21 liters)", isChecked: false) ,
    IngredientType(name: "Pilsener Malt (5 kg)", isChecked: false),
    IngredientType(name: "Wheat Malt (2 kg)", isChecked: false),
    IngredientType(name: "Sparge (30 liters)", isChecked: false),
    IngredientType(name: "Magnum Hop 11% (20 g)", isChecked: false),
    IngredientType(name: "Styrian Golding Celeia 5,5% (40 g)", isChecked: false),
    IngredientType(name: "Yeast: Wyeast 3724 Belgian Saison 210 ml", isChecked: false)
]

var worksBelgian = [
    TaskType(name: "Mash Phase",info:"Add Water: 21 lt", isDone: false ),
    TaskType(name: "Mash Phase 1",info: "Mashing time: 75 min", isDone: false),
    TaskType(name: "Mash Phase 2",info: "Temperature: 15 min @52°C & 60 min @65°C", isDone: false),
    TaskType(name: "Mash Phase 3",info: "Add Pilsener Malt", isDone: false),
    TaskType(name: "Mash Phase 4",info: "Add Wheat Malt", isDone: false),
  
    TaskType(name: "Boiling Phase",info: "Boiling Time: 60 min", isDone: false),
    TaskType(name: "Boiling Phase 1",info: "Add Magnum Hop 11% (When start to boil)", isDone: false),
    TaskType(name: "Boiling Phase 2",info: "Add Styrian Golding Celeia 5,5%", isDone: false),
    TaskType(name: "Fermentation Phase",info: "Fermentation: 24 °C 2 Days & +28 °C till the end of fermentation", isDone: false),
    TaskType(name: "Fermentation Phase 1",info: "Maturation: 1 week at 2 °C", isDone: false),
    TaskType(name: "Fermentation Phase 2",info: "Yeast: Wyeast 3724 Belgian Saison 210 ml", isDone: false)
]

//Lager-Czechpilsner
let materialsLager = [
    IngredientType(name: "Water (20 liters)", isChecked: false) ,
    IngredientType(name: "Pilsener Malt (5 kg)", isChecked: false),
    IngredientType(name: "Sparge (28 liters)", isChecked: false),
    IngredientType(name: "Czech Saaz 4.2% (50g + 20g + 20g)", isChecked: false),
    IngredientType(name: "Brown Sugar (165g)", isChecked: false),
    IngredientType(name: "Yeast: Wyeast 2001 Urquell Lager (190 ml)", isChecked: false)
]

var worksLager = [
    TaskType(name: "Mash Phase",info:"Add Water: 20 lt", isDone: false ),
    TaskType(name: "Mash Phase 1",info: "Mashing time: 60 min", isDone: false),
    TaskType(name: "Mash Phase 2",info: "Temperature: 60 min @65°C", isDone: false),
    TaskType(name: "Mash Phase 3",info: "Add Pilsener Malt", isDone: false),
    TaskType(name: "Boiling Phase",info: "Boiling Time: 60 min", isDone: false),
    TaskType(name: "Boiling Phase 1",info: "Add Czech Saaz", isDone: false),
    TaskType(name: "Fermentation Phase",info: "Add Brown Sugar", isDone: false),
    TaskType(name: "Fermentation Phase 1",info: "24 °C 2 Days & +28 °C till the end of fermentation", isDone: false),
    TaskType(name: "Fermentation Phase 2",info: "Maturation: 1 week at 2 °C", isDone: false),
    TaskType(name: "Fermentation Phase 2",info: "Yeast: Wyeast 2001 Urquell Lager", isDone: false)
]


let ipa = BeerType(name: "IPA", ingredients:materialsIpa, tasks: worksIpa, image: "ipa")

let stout = BeerType(name: "Stout", ingredients:materialsStout, tasks: worksStout, image: "stout")

let lager = BeerType(name: "Lager", ingredients:materialsLager, tasks: worksLager, image: "Lager")

let belgian = BeerType(name: "Belgian", ingredients:materialsBelgian, tasks: worksBelgian, image: "belgian")

let weisse = BeerType(name: "Weisse", ingredients:materialsWeisse, tasks: worksWeisse, image: "weisse")
