//
//  Protocol.swift
//  uBeer
//
//  Created by Domenico Varchetta on 07/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation

protocol FirstViewDelegate {
    func updateArray(projects: [Project])
}
