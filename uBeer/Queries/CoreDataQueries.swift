//
//  CoreDataQueries.swift
//  uBeer
//
//  Created by Tareq Saifullah on 20/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class CoreDataQueries{
    
    // Private initializer so that it has only on instance
    private init() {}
    
    //    Shared instance for the helper class So we can have only one instace throughtout the app
    
    
    static var shareInstance = CoreDataQueries()
    
    
    let context = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    
    // MARK: Product Queries
    
    //    Method to save the product data
    func saveProduct(obj:[String:String],ingredientObj:[IngredientType],taskObj:[TaskType]){
        
        let project = NSEntityDescription.insertNewObject(forEntityName: "Project", into: context!) as! Project
        project.name = obj["name"]
        let beer = NSEntityDescription.insertNewObject(forEntityName: "Beer", into: context!) as! Beer
        beer.name = obj["beer"]
        beer.image = obj["beerImage"]
        project.beer = beer
        for integ in ingredientObj {
            let ingredient = NSEntityDescription.insertNewObject(forEntityName: "Ingredient", into: context!) as! Ingredient
            ingredient.created = Date()
            ingredient.beer = beer
            ingredient.name = integ.name
            ingredient.isChecked = integ.isChecked
        }
        for tas in taskObj {
            let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context!) as! Task
            task.created = Date()
            task.beer = beer
            task.name = tas.name
            task.isDone = tas.isDone
            task.info = tas.info
        }
        
        do{
            try context?.save()
        }catch{
            print("Data not saved")
        }
    }
    func allProjects() -> [Project] {
        var projects = [Project]()
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Project")
        do {
            projects  = try context?.fetch(fetchRequest) as! [Project]
        } catch  {
            print("Error in fetching Projects")
        }
        return projects
    }
    
    func getProduct(projectName:String) -> [Ingredient] {
        var project=Project()
        var ingredients = [Ingredient]()
        
        do {
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Project")
            fetchRequest.predicate = NSPredicate(format: "name == %@", projectName)
            
            project = try context?.fetch(fetchRequest).first as! Project
            
            
            do {
                let fetchRequestIngredient = NSFetchRequest<NSManagedObject>(entityName: "Ingredient")
                fetchRequestIngredient.predicate = NSPredicate(format: "beer == %@", project.beer!.name!)
                ingredients = try context?.fetch(fetchRequestIngredient) as! [Ingredient]
            } catch  {
                print("Error in ingredient request")
            }
               } catch  {
                   print("Error in fetching Projects")
               }
        
        return ingredients
    }
    
    
    
    
    
    //
    //    func saveTask(obj:[String:Any],product:Product){
    //
    //        let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context!) as! Task
    //        task.name = obj["name"] as? String
    //        task.about = obj["about"] as? String
    //        task.time = obj["time"] as! Int64
    //        task.isDone = obj["isDone"] as! Bool
    //        task.addToProduct(product)
    //
    //        do{
    //            try context?.save()
    //        }catch{
    //            print("Data not saved")
    //        }
    //    }
    
    
    // MARK: Beer Queries
    
    func saveBeer(dictionary:[String:Any]){
        
        let beer = NSEntityDescription.insertNewObject(forEntityName: "Beer", into: context!) as! Beer
        beer.name = dictionary["name"] as? String
        do{
            try context?.save()
        }catch{
            print("Beer not saved")
        }
    }
    
    
    
    // MARK: Ingredients Queries
    
    func saveIngredient(dictionary:[String:Any],beer:Beer){
        
        let ingredient = NSEntityDescription.insertNewObject(forEntityName: "Ingredient", into: context!) as! Ingredient
        //        ingredient.beer = beer
        ingredient.name = dictionary["name"] as? String
        ingredient.isChecked = dictionary["isChecked"] as! Bool
        do{
            try context?.save()
        }catch{
            print("Data not saved")
        }
    }
    
    
    
    // MARK: Task Queries
    
    //    func saveTask(dictionary:[String:Any]){
    //
    //        let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context!) as! Task
    //
    //
    //        do{
    //            try context?.save()
    //        }catch{
    //            print("Data not saved")
    //        }
    //    }
    
    
    
    
}

