//
//  projectCell.swift
//  uBeer
//
//  Created by Domenico Varchetta on 21/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class projectCell: UICollectionViewCell {
    
    let projectImageView: UIImageView = {
        let iv = UIImageView()
        iv.widthAnchor.constraint(equalToConstant: 70).isActive = true
        iv.heightAnchor.constraint(equalToConstant: 70).isActive = true
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let projectTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Project Title"
        label.font = .systemFont(ofSize: 18)
        label.textColor = .black
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        let projectStackView = UIStackView(arrangedSubviews: [
            projectImageView, projectTitleLabel
        ])
        projectStackView.axis = .vertical
        projectStackView.alignment = .center
        projectStackView.distribution = .fill
        projectStackView.spacing = 12
        
        addSubview(projectStackView)
        projectStackView.fillSuperview(padding: .init(top: 20, left: 20, bottom: 10, right: 20))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
