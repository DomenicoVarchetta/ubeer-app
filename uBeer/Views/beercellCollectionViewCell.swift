import UIKit

class BeerCollectionViewCell: UICollectionViewCell {
    
    let projectImageView: UIImageView = {
        let iv = UIImageView()
        iv.widthAnchor.constraint(equalToConstant: 80).isActive = true
        iv.heightAnchor.constraint(equalToConstant: 90).isActive = true
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let projectTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Project Title"
        label.font = .systemFont(ofSize: 15)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        let stackView = UIStackView(arrangedSubviews: [
            projectImageView, projectTitleLabel
        ])
        stackView.axis = .vertical
        //stackView.distribution = .equalSpacing
//        stackView.distribution = .fillProportionally
        stackView.distribution = .fillEqually
        stackView.spacing = 5
        
        addSubview(stackView)
        stackView.fillSuperview(padding: .init(top: 20, left: 20, bottom: 0, right: 20))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
